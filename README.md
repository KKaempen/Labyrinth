# Labyrinth

This is a fork of another repository, specifically [this one](https://gitlab.com/fgoyal/Labyrinth). This is a final project that I worked on with four other students for a game development class that we were all taking at the University of Illinois. The game is not in a particularly well-finished state, but we're reasonably happy with what we were able to accomplish in the time we had. Because it's what the class focused on, the functionality of the game is coded in Unreal's blueprint editor, rather than directly in C++.

I was primarily in charge of coding much of the base functionality of the game. I wrote the code to create the maze that the game takes place in, implemented a "restart from checkpoint" system, implemented a pathfinding algorithm that would illuminate a path to lead you to the final room, implemented the logic for determining when that path would be illuminated, created a fog that would conceal unexplored rooms, and a few other portions of the game.

This was written in Unreal 4.27.2. To look at the blueprints you should be able to clone the repository (this uses the Git LFS system for the larger files) and then import the project into Unreal.
